// #
// # Modbus Configuration
// #
#define CONFIG_MB_UART_PORT_NUM 1
#define CONFIG_MB_UART_BAUD_RATE115200
#define CONFIG_MB_UART_RXD 5
#define CONFIG_MB_UART_TXD 4
#define CONFIG_MB_UART_RTS 18
#define CONFIG_MB_COMM_MODE_RTU y